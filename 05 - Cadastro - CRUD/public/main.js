let list = document.querySelector("#container ul");
let table = document.querySelector("#container #usersTable");
let userArray = [];
let userLastLogin = {};

const URL = `http://localhost:3333`;

function createDeleteLink(userId, row) {
  let deleteLink = document.createElement("a");
  deleteLink.setAttribute("href", "#");
  deleteLink.textContent = "Excluir";
  deleteLink.setAttribute("id", "deleteUser");
  deleteLink.addEventListener("click", function(e) {
    e.preventDefault();
    axios.post(URL + "/delete", { userId }).then(({ data }) => {
      table.removeChild(row);
      userArray = userArray.filter(element => {
        if (element != row) {
          return element;
        }
      });
    });
  });
  return deleteLink;
}

function createUpdateLink(user) {
  let updateLink = document.createElement("a");
  updateLink.setAttribute("href", "#");
  updateLink.textContent = "Atualizar";
  updateLink.setAttribute("id", "updateUserLink");
  updateLink.addEventListener("click", function(e) {
    e.preventDefault();
    let updateDiv = document.querySelector("#container #updateUser");
    console.log(updateDiv);

    updateDiv.removeAttribute("hidden");

    document.querySelector("#container #updateUser #nomeUpdate").value =
      user.nome_completo;
    document.querySelector("#container #updateUser #loginUpdate").value =
      user.login;
    document.querySelector("#container #updateUser #cidadeUpdate").value =
      user.cidade;

    userLastLogin = user.login;
  });
  return updateLink;
}

function createUserEvent() {
  let button = document.querySelector("#container #btInsert");
  button.addEventListener("click", () => {
    let nome = document.querySelector("#container #nome");
    let login = document.querySelector("#container #login");
    let cidade = document.querySelector("#container #cidade");

    axios
      .post(URL + "/create", {
        nome: nome.value,
        login: login.value,
        cidade: cidade.value
      })
      .then(({ data }) => {
        if (userArray.length > 0) {
          let buttonSearch = document.querySelector("#container #btSearch");
          buttonSearch.dispatchEvent(new Event("click"));
        }
        console.log(data);
        nome.value = "";
        login.value = "";
        cidade.value = "";
      });
  });
}

function listUserevent() {
  let button = document.querySelector("#container #btSearch");

  button.addEventListener("click", () => {
    axios.get(URL + "/read").then(({ data }) => {
      userArray.forEach(element => {
        table.removeChild(element);
      });
      userArray = [];
      data.forEach(element => {
        let tr = document.createElement("tr");
        for (const key of Object.keys(element)) {
          let td = document.createElement("td");
          td.textContent = element[key];
          tr.appendChild(td);
        }
        let tdUpdate = document.createElement("td");
        tdUpdate.appendChild(createUpdateLink(element, tr));
        let tdDelete = document.createElement("td");
        tdDelete.appendChild(createDeleteLink(element["login"], tr));
        tr.appendChild(tdDelete);
        tr.appendChild(tdUpdate);
        userArray.push(tr);
        table.appendChild(tr);
      });
    });
  });
}

function updateUserEvent() {
  let button = document.querySelector(
    "#container #updateUser #updateUserButton"
  );
  button.event;
  button.addEventListener("click", () => {
    let updateDiv = document.querySelector("#container #updateUser");

    let nome = document.querySelector("#container #updateUser #nomeUpdate");
    let login = document.querySelector("#container #updateUser #loginUpdate");
    let cidade = document.querySelector("#container #updateUser #cidadeUpdate");
    const form = {
      nome: nome.value,
      login: login.value,
      cidade: cidade.value,
      userLastLogin
    };

    axios.post("/update", form).then(({ data }) => {
      let buttonSearch = document.querySelector("#container #btSearch");
      buttonSearch.dispatchEvent(new Event("click"));
      nome.value = "";
      login.value = "";
      cidade.value = "";
      updateDiv.setAttribute("hidden", "true");
    });
  });
}

function main() {
  createUserEvent();
  listUserevent();
  updateUserEvent();
}

main();
