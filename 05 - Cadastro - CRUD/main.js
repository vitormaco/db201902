const express = require("express");
require("dotenv").config();
const { Pool, Client } = require("pg");
const cors = require("cors");

const app = express();
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(express.static(`./public`));

const client = new Client();
client.connect();

app.post("/create", (req, res) => {
  client
    .query(`
      INSERT INTO Usuario VALUES('${req.body.nome}','${req.body.login}','${req.body.cidade}');
    `)
    .then(result => {
      res.send("OK");
    });
});

app.get("/read", (req, res) => {
  client
    .query(`
      SELECT * FROM Usuario;
    `)
    .then(result => {
      res.send(result.rows);
    });
});

app.post("/delete", (req, res) => {
  client
    .query(`
      DELETE FROM Usuario
      WHERE login='${req.body.userId}';
    `)
    .then(result => {
      res.send(result.rows);
    });
});

app.post("/update", (req, res) => {
  const { login, cidade, nome, userLastLogin } = req.body;
  client
    .query(`
      UPDATE Usuario
      SET
      login='${login}',
      cidade='${cidade}',
      nome_completo='${nome}'
      WHERE login='${userLastLogin}';
    `)
    .then(result => {
      res.send("OK");
    });
});

const server = app.listen(process.env.PORT || 3000, () =>
  console.log(`Running at port ${server.address().port}`)
);
