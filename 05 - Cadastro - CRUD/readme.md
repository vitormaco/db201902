# CRUD Banco de Dados 2019

## O arquivo principal para o código é o 'main.js'
### Cada uma das rotas é uma operação do CRUD

## Segue alguns comandos úteis para executar a aplicação.

## Executar os comandos dentro da pasta "src"

### Instalar pacotes

- npm install

### Iniciar versão para debugar (Com live reaload)

- npm run dev

### Iniciar versão otimizada

- npm start

### Servidor vai rodar por padrão na porta 3333, para alterar basta mudar o valor da variavel PORT no arquivo ".env".
