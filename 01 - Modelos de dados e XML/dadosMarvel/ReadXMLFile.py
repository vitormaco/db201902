#!/usr/bin/python
from xml.dom.minidom import parse
import xml.dom.minidom
import math
import csv

DOMTree = xml.dom.minidom.parse("marvel_simplificado.xml")
universe = DOMTree.documentElement

all_heroes = universe.getElementsByTagName("hero")
atributos = ["name", "popularity", "alignment", "gender", "height_m", "weight_kg", "hometown", 
    "intelligence", "strength", "speed", "durability", "energy_Projection", "fighting_Skills"]

heroes_array = []

for hero in all_heroes:
    hero_array = {}
    if hero.hasAttribute("id"):
        hero_array['id'] = hero.getAttribute("id")
        for atributo in atributos:
            hero_array[str(atributo)] = hero.getElementsByTagName(atributo)[0].childNodes[0].data

    heroes_array.append(hero_array)


good_heroes = [hero for hero in heroes_array if hero['alignment'] == 'Good']
bad_heroes = [hero for hero in heroes_array if hero['alignment'] == 'Bad']


with open('herois.csv', mode='w') as csv_file:
    atributos.append('id')
    writer = csv.DictWriter(csv_file, fieldnames=atributos)
    for hero in heroes_array:
        writer.writerow(hero)

with open('herois_good.csv', mode='w') as csv_file:
    atributos.append('id')
    writer = csv.DictWriter(csv_file, fieldnames=atributos)
    for hero in good_heroes:
        writer.writerow(hero)

with open('herois_bad.csv', mode='w') as csv_file:
    atributos.append('id')
    writer = csv.DictWriter(csv_file, fieldnames=atributos)
    for hero in bad_heroes:
        writer.writerow(hero)

print("Proporção de heróis Bons/Maus: " + str(len(good_heroes) / len(bad_heroes)))

weight_array = [hero['weight_kg'] for hero in heroes_array]
print("Média de peso dos Heróis: " + str(sum(map(int, weight_array)) / len(weight_array)))

hulk = list(hero for hero in heroes_array if hero['name'] == 'Hulk')[0]
print("IMC do Hulk: " + str(int(hulk['weight_kg']) / (int(hulk['height_m'])**2)))
