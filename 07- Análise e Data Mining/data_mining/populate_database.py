from extract_from_postgres import get_data_from_postgres
from mongoConnection import mongo_connect
import pprint
import requests
import json
import pymongo

SPOTIFY_B64_SECRET = 'MGY3MWZiZGE4ZjdmNDE4MmI4N2QyMDJjMzE2NjRjNjg6YzRiZDYzMGJjMDU4NDRhNjljMmQ4ZjI0NmY1MmY4YTI='
IMDB_API_KEY = 'da9ff8a5'
progress_bar_count = 0
progress_bar_total = 0


def get_spotify_auth_token():
    response = requests.post(
        'https://accounts.spotify.com/api/token',
        headers={'Authorization': 'Basic ' + SPOTIFY_B64_SECRET},
        data={'grant_type': 'client_credentials'},
    )
    decoded_response = eval(response.content.decode())
    return decoded_response['access_token']


def get_artists_info(artists):
    artists = list(set(artists))
    artists.sort()
    spotify_auth = get_spotify_auth_token()
    set_progress_bar_total(len(artists))
    print(' - getting info on '+str(len(artists))+' artists')
    artists_info = [get_artist_info(spotify_auth, artist)
                    for artist in artists]
    print(' ✔ retrieved artists info successfully')
    return artists_info


def get_artist_info(token, artist):
    increment_progress_bar()
    print_progress_bar()

    response = requests.get(
        'https://api.spotify.com/v1/search',
        headers={'Authorization': 'Bearer ' + token},
        params={'q': artist, 'type': 'artist', 'limit': 1},
    )
    artist = json.loads(response.content.decode())['artists']['items'][0]
    return {

        "name": artist['name'],
        "genres": artist['genres'],
        "image": artist['images'][0]['url'] if artist['images'] else None,
        "popularity": artist['popularity'],
    }


def get_movies_info(movies):
    movies = list(set(movies))
    movies.sort()

    set_progress_bar_total(len(movies))
    print(' - getting info on '+str(len(movies))+' movies')
    movies_info = [get_movie_info(movie) for movie in movies]
    print(' ✔ retrieved movies info successfully')
    return movies_info


def get_movie_info(movie):
    increment_progress_bar()
    print_progress_bar()
    movie_id = movie.split("/")[-2]
    response = requests.get(
        'http://www.omdbapi.com/?i='+movie_id+'&apikey=da9ff8a5'
    )
    movie = json.loads(response.content.decode())

    return {
        'id': movie_id,
        'title': movie['Title'],
        'genre': movie['Genre'],
        'actors': movie['Actors'],
        'year': movie['Year'],
        'country': movie['Country'],
        'director': movie['Director'],
        'image': movie['Poster'],
    }


def increment_progress_bar():
    global progress_bar_count
    progress_bar_count += 1


def set_progress_bar_total(value):
    global progress_bar_total
    progress_bar_total = value


def print_progress_bar(prefix='', suffix='', decimals=1, length=100, fill='█', printEnd="\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """

    global progress_bar_count
    global progress_bar_total

    percent = ("{0:." + str(decimals) + "f}").format(100 *
                                                     (progress_bar_count / float(progress_bar_total)))
    filledLength = int(length * progress_bar_count // progress_bar_total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end=printEnd)
    # Print New Line on Complete
    if progress_bar_count == progress_bar_total:
        print()


if __name__ == "__main__":
    print(' === retrieving data from postgres === ')
    dataset = get_data_from_postgres()
    print(' === data retrieved from postgres successfully === \n')

    client = pymongo.MongoClient(
        "mongodb://powerpuff:NGWJEqYHIhg7IfQv@cluster0-shard-00-00-opo6q.gcp.mongodb.net:27017,cluster0-shard-00-01-opo6q.gcp.mongodb.net:27017,cluster0-shard-00-02-opo6q.gcp.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority")
    db = client["PowerPuffDB"]
    artists_col = db.artists
    movies_col = db.movies
    person_col = db.person

    data_list = []
    artists = []
    movies = []
    for data in dataset:
        data_list.append({
            "name": data,
            "hometown": dataset[data]["hometown"],
            "birthdate": dataset[data]["birthdate"],
            "artists": dataset[data]["artists"],
            "knows": dataset[data]["knows"],
            "movies": dataset[data]["movies"],
        })
       # artists += [elem['artist'] for elem in dataset[data]["artists"]]
       # movies += [elem['movieurl'] for elem in dataset[data]["movies"]]

    # person_col.insert_many(data_list)
    print(' === retrieving data from external datasources === ')
    artists = get_artists_info(artists)
    pprint.pprint(artists)
    # artists_col.insert_many(artists)

    movies = get_movies_info(movies)
    pprint.pprint(movies)
    # movies_col.insert_many(movies)
    print(' === data retrieved from external datasources successfully === ')

    # pprint.pprint(data_list, depth=2)
