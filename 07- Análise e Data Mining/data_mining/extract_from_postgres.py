import psycopg2
from datetime import datetime
from urllib.parse import unquote


def get_data_from_postgres():
    print(' - establishing connection to postgres database')
    connection = psycopg2.connect(host='200.134.10.32', database='201902PowerPuff_DBgirls',
                                  user='201902PowerPuff_DBgirls', password='300005')
    cursor = connection.cursor()
    print(' ✔ connection established')

    print(' - retrieving persons info')
    persons = get_persons(cursor)
    print(' ✔ persons info successfully retrieved')

    print(' - retrieving movies info')
    movies = get_movies(cursor)
    print(' ✔ movies info successfully retrieved')

    print(' - retrieving artists info')
    artists = get_artists(cursor)
    print(' ✔ artists info successfully retrieved')

    for movie in movies:
        persons[movie["person_name"]]["movies"].append({
            "movieurl": movie["movieurl"],
            "rating": movie["rating"],
        })

    for artist in artists:
        artist_name = unquote(artist["bandurl"]).split(
            "/")[-1].split("_(")[0].replace("_", " ")
        if all(ord(c) < 128 for c in artist_name):
            persons[artist["person_name"]]["artists"].append({
                "bandurl": artist["bandurl"],
                "artist": artist_name,
                "rating": artist["rating"],
            })

    print(' - retrieving relationship info')
    cursor.execute(
        'select p.name, p2.name from knows k join person p on p.uri = k.person join person p2 on p2.uri = k.colleague')
    for colleague in cursor:
        persons[colleague[0]]["knows"].append(
            colleague[1],
        )
    print(' ✔ relationship info successfully retrieved')

    cursor.close()
    connection.close()
    return persons


def get_persons(cursor):
    cursor.execute('select name, hometown, birthdate from person')
    persons = {}
    for person in cursor:
        persons.update({
            person[0]: {
                "hometown": person[1],
                "birthdate": (person[2]).strftime("%Y-%m-%d") if person[2] else '',
                "movies": [],
                "artists": [],
                "knows": [],
            }
        })
    return persons


def get_movies(cursor):
    cursor.execute(
        'select name, rating, movieuri from likesmovie m join person p on m.person = p.uri')
    movies = []
    for movie in cursor:
        p = {
            "person_name": movie[0],
            "rating": movie[1],
            "movieurl": movie[2],
        }
        movies.append(p)
    return movies


def get_artists(cursor):
    cursor.execute(
        'select name, rating, banduri from likesmusic m join person p on m.person = p.uri')
    artists = []
    for artist in cursor:
        p = {
            "person_name": artist[0],
            "rating": artist[1],
            "bandurl": artist[2],
        }
        artists.append(p)
    return artists
