import React, { Component } from 'react';
import { Row, Col, Card } from 'react-materialize';
import UserInfo from '../components/UserInfo';

class Home extends Component {
    render() {
        if(!localStorage.username) {
            this.props.history.push("/")
        }
        return (
            <Row>
                <Col m={3} s={12}>
                    <UserInfo username={localStorage.username}/>
                </Col>
                <Col m={8} s={12}>
                    {/*Aqui vai a descrição do jogo ou alguma informação relevante
                    como editar algo do teu cadastro*/}
                    <h5 className="subtitle">About the Game</h5>
                    <Card>
                        <div>
                            <p><b>Joguinho de Banco de dados</b></p>
                            <p>Esse eh o joguinho q a gnt fez pro curso de banco de dados</p>
                        </div>
                    </Card>
                </Col>
            </Row>
        );
    }
}

export default Home;