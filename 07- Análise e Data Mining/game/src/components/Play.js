import React, { Component } from "react";
import { Row, Col, Card, Button } from "react-materialize";
import UserInfo from "../components/UserInfo";
import Axios from "axios";

let timerCounter = 100;
let score = 0;
let question = 1;

const image_answers_question_type = [1, 4];

const buttonStyle = {
    width: "49%",
    margin: "5px 0"
};

class Play extends Component {
    constructor(props) {
        super(props);
        this.question = null;
    }

    timer = setInterval(() => {
        const timerElem = document.getElementById("timer");
        if (timerElem) {
            timerElem.innerHTML = timerCounter > 20 ? timerCounter-- : 20;
        }
    }, 100);

    buttonStyle = {
        width: "49%",
        margin: "5px 0"
    };

    updateQuestionNumber() {
        const questNumb = document.getElementById("question_number");
        if (questNumb) {
            questNumb.innerHTML = "Question: " + question;
        }
    }

    updateScore() {
        const scoreElem = document.getElementById("score");
        if (scoreElem) {
            scoreElem.innerHTML = "Score: " + score;
        }
    }

    handleAnswer = (c, a) => {
        this.forceUpdate();
        if (c === a) {
            alert("acertou!");
            score += timerCounter;
        } else {
            alert("errou!");
        }
        question++;
        this.updateScore();
        this.updateQuestionNumber();
        this.loadQuestion();
    };

    async loadQuestion() {
        await Axios.post("http://vitormaco.me:5000/getRandomQuestion", {
            username: localStorage.getItem("username")
        }).then(({ data }) => {
            const rand = Math.floor(Math.random() * 4);
            const ans = data.answer.toString().split(",");
            const itm = data.items.toString().split(",");
            const res = [];
            for (let i = 0; i < 4; i++) {
                if (i == rand) res[i] = ans.shift();
                else res[i] = itm.shift();
            }
            this.question = {
                data: data,
                loading: false,
                answers: res,
                question_type: data.question_type
            };
            timerCounter = 100;
            if (question == 6) {
                Axios.post("http://vitormaco.me:5000/savepoint", {
                    username: localStorage.getItem("username"),
                    score: score,
                    question_type: 0
                });

                // tem que criar uma pagina escrito, obrigado por jogar bla bla bla e colocar aqui
                this.props.history.push("/home");
                return;
            }

            console.log(
                "fazer aqui um post para salvar esse placar no ranking, e redirecionar o jogador"
            );
            this.forceUpdate();
        });
    }

    componentDidMount() {
        this.loadQuestion();
    }

    render() {
        if (!localStorage.username) {
            this.props.history.push("/");
        }
        if (!this.question) {
            return <h1>Loading</h1>;
        }
        return (
            <Row>
                <Col m={2} s={12}>
                    <h5>You</h5>
                    <UserInfo />
                    <h5 id="question_number">Question: 1</h5>
                    <h5 id="score">Score: 0</h5>
                </Col>
                <Col m={5} s={12}>
                    <div
                        style={{
                            display: "flex",
                            justifyContent: "space-between"
                        }}
                    >
                        <h5 className="subtitle">Question</h5>
                        <h5 id="timer" />
                    </div>
                    <Card>
                        {this.question.data.image && (
                            <Row>
                                <Col s={8} m={8} offset="s2 m2">
                                    <img
                                        src={this.question.data.image}
                                        className="square responsive-img"
                                    />
                                    }
                                </Col>
                            </Row>
                        )}
                        <div>
                            <p>
                                <b>{this.question.data.question}</b>
                            </p>
                        </div>
                    </Card>
                </Col>
                <Col m={5} s={12}>
                    <h5 className="subtitle">Your answer</h5>
                    <Card>
                        <div>
                            <p>
                                <b>What is the right answer?</b>
                            </p>
                            {/*Arrumar depois*/}
                            {!image_answers_question_type.includes(
                                this.question.question_type
                            ) ? (
                                <>
                                    <Button
                                        onClick={() =>
                                            this.handleAnswer(
                                                this.question.answers[0],
                                                this.question.data.answer
                                            )
                                        }
                                        style={buttonStyle}
                                    >
                                        {this.question.answers[0]}
                                    </Button>{" "}
                                    <Button
                                        onClick={() =>
                                            this.handleAnswer(
                                                this.question.answers[1],
                                                this.question.data.answer
                                            )
                                        }
                                        style={buttonStyle}
                                    >
                                        {this.question.answers[1]}
                                    </Button>{" "}
                                    <Button
                                        onClick={() =>
                                            this.handleAnswer(
                                                this.question.answers[2],
                                                this.question.data.answer
                                            )
                                        }
                                        style={buttonStyle}
                                    >
                                        {this.question.answers[2]}
                                    </Button>{" "}
                                    <Button
                                        onClick={() =>
                                            this.handleAnswer(
                                                this.question.answers[3],
                                                this.question.data.answer
                                            )
                                        }
                                        style={buttonStyle}
                                    >
                                        {this.question.answers[3]}
                                    </Button>
                                </>
                            ) : (
                                <>
                                    <Card>
                                        <Row>
                                            <Col s={8} m={8} offset="s2 m2">
                                                <img
                                                    src={
                                                        this.question.answers[0]
                                                    }
                                                    className="square responsive-img"
                                                />
                                            </Col>
                                        </Row>
                                        <Button
                                            onClick={() =>
                                                this.handleAnswer(
                                                    this.question.answers[0],
                                                    this.question.data.answer
                                                )
                                            }
                                            style={buttonStyle}
                                        >
                                            Selecionar
                                        </Button>
                                    </Card>{" "}
                                    <Card>
                                        <Row>
                                            <Col s={8} m={8} offset="s2 m2">
                                                <img
                                                    src={
                                                        this.question.answers[1]
                                                    }
                                                    className="square responsive-img"
                                                />
                                            </Col>
                                        </Row>
                                        <Button
                                            onClick={() =>
                                                this.handleAnswer(
                                                    this.question.answers[1],
                                                    this.question.data.answer
                                                )
                                            }
                                            style={buttonStyle}
                                        >
                                            Selecionar
                                        </Button>
                                    </Card>{" "}
                                    <Card>
                                        <Row>
                                            <Col s={8} m={8} offset="s2 m2">
                                                <img
                                                    src={
                                                        this.question.answers[2]
                                                    }
                                                    className="square responsive-img"
                                                />
                                            </Col>
                                        </Row>
                                        <Button
                                            onClick={() =>
                                                this.handleAnswer(
                                                    this.question.answers[2],
                                                    this.question.data.answer
                                                )
                                            }
                                            style={buttonStyle}
                                        >
                                            Selecionar
                                        </Button>
                                    </Card>{" "}
                                    <Card>
                                        <Row>
                                            <Col s={8} m={8} offset="s2 m2">
                                                <img
                                                    src={
                                                        this.question.answers[3]
                                                    }
                                                    className="square responsive-img"
                                                />
                                            </Col>
                                        </Row>
                                        <Button
                                            onClick={() =>
                                                this.handleAnswer(
                                                    this.question.answers[3],
                                                    this.question.data.answer
                                                )
                                            }
                                            style={buttonStyle}
                                        >
                                            Selecionar
                                        </Button>
                                    </Card>
                                </>
                            )}
                        </div>
                    </Card>
                </Col>
            </Row>
        );
    }
}

export default Play;
