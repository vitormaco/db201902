import React, { Component } from 'react';
import { Row, Col, Card } from 'react-materialize';
import avatar from '../img/avatar.jpg';
class UserProfile extends Component {
    render() {
        if(!localStorage.username) {
            this.props.history.push("/")
        }
        return (
            <Card>
                <Row>
                    <Col s={4}>
                        <img src={avatar} className="square responsive-img" alt='' />
                    </Col>
                </Row>
                {/* colocar aqui info sobre a pessoa */}
                <Row className="center-align">
                    <h5>{this.props.username}</h5>
                    <p className="grey darken-2 white-text">Description</p>
                </Row>
            </Card>
        );
    }
}

export default UserProfile;