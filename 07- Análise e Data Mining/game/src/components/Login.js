import React, { Component } from "react";
import { Row, Col, Card, Input, Button } from "react-materialize";
import { withRouter } from "react-router-dom";
import Axios from "axios";

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = { value: "" };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        this.setState({ value: e.target.value });
    }

    handleSubmit = async e => {
        e.preventDefault();
        try {
            await Axios.post("http://vitormaco.me:5000/login", {
                username: this.state.value
            }).then(({ data }) => {
                console.log(data);

                if (data.user !== null) {
                    alert("Você está logado como " + data.user);
                    localStorage.setItem("username", data.user);
                    this.props.history.push("/home");
                } else {
                    alert("Usuário incorreto.");
                }
            });
        } catch (err) {
            console.log(err);
        }
    };

    async getUsers() {
        await Axios.get("http://vitormaco.me:5000/listUsers").then(
            ({ data }) => {
                this.users = data.users;
                this.forceUpdate();
            }
        );
    }

    userOptions() {
        if (this.users) {
            return (
                <>
                    {this.users.map(user => {
                        return (
                            <option key={user} value={user}>
                                {user}
                            </option>
                        );
                    })}
                </>
            );
        }
        return (
            <option key="" value="null">
                loading names from database
            </option>
        );
    }

    componentDidMount() {
        this.getUsers();
    }

    render() {
        return (
            <Row>
                <Col m={8} s={12}>
                    <h5>Login</h5>
                    <Card>
                        <Row>
                            <form onSubmit={this.handleSubmit}>
                                <Input
                                    s={12}
                                    type="select"
                                    label="Select"
                                    onChange={this.handleChange}
                                >
                                    <option value="null">
                                        Select your name
                                    </option>
                                    {this.userOptions()}
                                </Input>

                                <Button type="submit">login</Button>
                            </form>
                        </Row>
                    </Card>
                </Col>
            </Row>
        );
    }
}

export default withRouter(Login);
