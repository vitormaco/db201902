import React, { Component } from "react";
import { Row, Col, Card, Button } from "react-materialize";
import Axios from "axios";

class Ranking extends Component {
    constructor(props) {
        super(props);
        this.ranking = null;
    }

    async loadRanking() {
        await Axios.get("http://vitormaco.me:5000/getranking").then(
            ({ data }) => {
                this.ranking = data;
                console.log(data);

                this.ranking.ranking = this.ranking.ranking.sort(
                    (a, b) => b.score - a.score
                );

                let objCheck = {};
                let final_rank = [];
                this.ranking.ranking.forEach(e => {
                    if (!(e.score in objCheck)) {
                        final_rank.push(e);
                        objCheck[e.score] = 1;
                    }
                });
                this.ranking.ranking = final_rank;

                this.forceUpdate();
            }
        );
    }

    getRanking() {
        if (this.ranking) {
            console.log(this.ranking);
            let i = 0;
            return (
                <div>
                    {this.ranking.ranking.map(data => {
                        return (
                            <div
                                key={i++}
                                style={{
                                    display: "flex",
                                    justifyContent: "space-between"
                                }}
                            >
                                <p>{data["name"]}</p>
                                <p> score: {data["score"]}</p>
                            </div>
                        );
                    })}
                </div>
            );
        }
        return <p>carregando</p>;
    }

    componentDidMount() {
        this.loadRanking();
    }

    render() {
        return (
            <div>
                <Card>
                    <Row>{this.getRanking()}</Row>
                </Card>
            </div>
        );
    }
}

export default Ranking;
