import React, { Component } from 'react';
import { Navbar, Row} from 'react-materialize';
import { NavLink } from 'react-router-dom'

class Header extends Component {
    render() {
        return (
            <Row>
                <Navbar className="blue darken-3">
                    <li><NavLink to='/home'>Home</NavLink></li>
                    <li><NavLink to='/play'>Play</NavLink></li>
                    <li><NavLink to='/profile'>Profile</NavLink></li>
                    <li><NavLink to='/ranking'>Ranking</NavLink></li>
                    <li><NavLink to='/logout'>Log out</NavLink></li>
                </Navbar>
            </Row>
        );
    }
}

export default Header;