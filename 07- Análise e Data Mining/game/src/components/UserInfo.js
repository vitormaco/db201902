import React, { Component } from 'react';
import { Row, Col, Card } from 'react-materialize';
import avatar from '../img/avatar.jpg';
class UserInfo extends Component {
    render() {
        return (
            <Card>
                <Row>
                    <Col s={8} m={8} offset="s2 m2">
                    <img src={avatar} className="square responsive-img" alt='' />
                    </Col>
                </Row>
                <Row className="center-align">
                    <h5>{this.props.username}</h5>
                    <p className="grey darken-2 white-text">{localStorage.username}</p>
                </Row>
            </Card>
        );
    }
}

export default UserInfo;