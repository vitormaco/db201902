import React, { Component } from 'react';
import { Row, Col, Card } from 'react-materialize';
import UserInfo from './UserInfo';

class Logout extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        localStorage.removeItem("username")
        this.props.history.push("/")
        return null;
    }
}

export default Logout;