import React from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import Login from "./components/Login";
import Home from "./components/Home"
import Header from "./components/Header";
import UserProfile from "./components/UserProfile";
import Ranking from "./components/Ranking";
import Logout from "./components/Logout";
import Play from "./components/Play";

const Routes = () => (
    <BrowserRouter>
        <Header />
        <Switch>
            <Route exact path="/" component={Login}/>
            <Route path="/home" component={Home} />
            <Route path="/ranking" component={Ranking} />
            <Route path="/play" component={Play} />
            <Route path="/profile" component={UserProfile} />
            <Route path="/logout" component={Logout} />
            // redirectiona 404s para o root/login
            <Redirect to='/' />
        </Switch>
    </BrowserRouter>
);

export default Routes;