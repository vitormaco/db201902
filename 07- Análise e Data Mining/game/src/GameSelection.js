import React, { Component } from 'react';
import styled from 'styled-components';

const merge_color = '0, 0, 255';

const Button = styled.button`
    background: rgba(${merge_color}, 0.3);
    color: #111111;
    font-size: 1em;
    margin: 2rem;
    padding: 0.25em 1em;
    border: 2px solid #111111;
    border-radius: 3px;
    :hover{background: rgba(${merge_color}, 0.5);}

    :focus{background: rgba(${merge_color}, 0.7);}

`;

const Title = styled.div`
    padding: 1.5rem 1.5rem 1.5rem 1.5rem;
    background: rgba(${merge_color}, 0.5);
`;

const Content = styled.div`
    background: rgba(${merge_color}, 0.1);
    padding: 5rem 10rem 5rem 10rem;
`;

const QuestionGame = styled.div`
    font-weight: bold;
    font-size: 1.5rem;
    text-align: center;
    color: #111111;
`;

const LoggedAs = styled.div`
    font-style: italic;
    font-size: 1rem;
    text-align: left;
    color: #111111;
    position: relative;
`;

const LogoutButton = styled.div`
    font-style: italic;
    font-size: 1rem;
    text-align: right;
    color: #111111;
    position: relative;
`;

class GameSelection extends Component {
    render() {
        return (
            <QuestionGame>   
                <Title>Choose your Game Mode:</Title>
                <LoggedAs>logged as: user123</LoggedAs>
                <LogoutButton>log out</LogoutButton>
                <Content>
                    <Button>Play Alone</Button>
                    <Button>Play with Friends</Button>
                    <Button>Play with Random People</Button>
                </Content>
            </QuestionGame>
        );
    }
}

export default GameSelection;