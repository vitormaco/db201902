from templates import get_music_questions
from templates import get_movie_questions
from templates import get_friends_questions
from templates import populate_general_information
from flask import Flask, request, jsonify
from flask_cors import CORS, cross_origin
import pprint
import psycopg2
import pymongo
import random
app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'


connection = psycopg2.connect(host='200.134.10.32', database='201902PowerPuff_DBgirls',
                              user='201902PowerPuff_DBgirls', password='300005')
cursor = connection.cursor()

client = pymongo.MongoClient(
    "mongodb://powerpuff:NGWJEqYHIhg7IfQv@cluster0-shard-00-00-opo6q.gcp.mongodb.net:27017,cluster0-shard-00-01-opo6q.gcp.mongodb.net:27017,cluster0-shard-00-02-opo6q.gcp.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority")
db = client["PowerPuffDB"]
artists_col = db.artists
movies_col = db.movies
person_col = db.person
ranking_col = db.ranking


@app.route('/login', methods=['POST'])
@cross_origin()
def login():
    data = request.json
    data_name = data["username"].lower()
    cursor.execute(
        """select name from person where LOWER(name) = %(str)s""", {"str": data_name})

    for person in cursor:
        if person[0].lower() == data["username"].lower():
            return {"user": person[0]}
    else:
        return {"user": None}


@app.route('/listUsers', methods=['GET'])
@cross_origin()
def users():
    cursor.execute('select name from person')
    return {'users': [person[0] for person in cursor]}
    return ''.join(person + '\n' for person in map(str, [person[0] for person in cursor]))


@app.route('/savepoint', methods=['POST'])
@cross_origin()
def savepoint():
    data = request.json
    data_name = data["username"]
    score = data["score"]
    question_type = data["question_type"]

    ranking_col.insert_one({"name": data_name, "score": score,
                            "question_type": question_type})

    return {"success": True}


@app.route('/getscore', methods=['POST'])
@cross_origin()
def getscore():
    data = request.json
    data_name = data["username"]

    user_rank = list(ranking_col.find({"name": data_name}))

    total_score = 0

    for rank in user_rank:
        total_score += int(rank["score"])

    return {"score": total_score}


@app.route('/getranking', methods=['GET'])
@cross_origin()
def getranking():
    ranking = list(ranking_col.find({}, {"_id": 0}))

    return {"ranking": ranking}


# POST PARA ESSA API.
# curl -i -H "Content-Type: application/json" -X POST -d '{"username": "Anderson Candido de Jesus da Silva", "score":1, "question_type": 2}' http://vitormaco.me:5000/getscore
@app.route('/getRandomQuestion', methods=['POST'])
@cross_origin()
def get_question():
    data = request.json
    data_name = data["username"]

    # busca usuário na base do mongo.
    user_result = person_col.find_one({"name": data_name})

    # usado para perguntas de filme que o usuário gosta
    user_likes_movie = [movies_col.find_one(
        {"id": movie["movieurl"].split("/")[-2]}) for movie in user_result["movies"]]

    # usado para perguntas de musica que o usuário gosta
    user_likes_music = [artists_col.find_one(
        {"id": artist["bandurl"]}) for artist in user_result["artists"]]

    # usado para perguntas sobre os amigos do usuário
    user_friends_info = [person_col.find_one(
        {"name": person}) for person in user_result["knows"]]

    # informações gerais para popular alternativas as questões
    artists = artists_col.find()
    movies = movies_col.find()

    # Talvez fazer um mining geral... mas pode ficar bem pesado
    persons = person_col.find()

    # informações sobre os amigos. Incluindo gostos de filmes e musicas.
    friends_mining = []
    for friend in user_friends_info:
        friends_mining.append(
            {"name": friend["name"],
             "likes_movie": [movies_col.find_one(
                 {"id": movie["movieurl"].split("/")[-2]}) for movie in friend["movies"]],
             "likes_music": [artists_col.find_one(
                 {"id": artist["bandurl"]}) for artist in friend["artists"]],
             "hometown": friend["hometown"],
             "birthdate": friend["birthdate"],
             "knows": friend["knows"]
             }
        )
    # somente popula o namespace global do templates.py para facilitar.
    populate_general_information(artists, movies, friends_mining, persons)

    question_type = random.randint(0, 2)

    # Pode incluir mais ifs aqui, por enquanto imagino que 0 = musicas do usuario, 1 = filmes do usuario, 2 = perguntas sobre gostos dos amigos.
    if question_type == 0:
        music_questions = get_music_questions()
        question_index = random.randint(0, len(music_questions)-1)
        return music_questions[question_index](user_likes_music)
    if question_type == 1:
        movie_questions = get_movie_questions()
        question_index = random.randint(0, len(movie_questions)-1)
        return movie_questions[question_index](user_likes_movie)
    else:
        friends_questions = get_friends_questions()
        question_index = random.randint(0, len(friends_questions)-1)
        return friends_questions[question_index]()


if __name__ == '__main__':
    app.run()
