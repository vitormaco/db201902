# acho que a ideia pode ser gerar varios modelos de perguntas aqui. E adicionar no array da "get_questions"
# assim, usamos um random na main que irá retornar alguma indice no array, que possui funções para gerar questoes especificas.
import random
import requests

mining = None
artists = None
movies = None
persons = None


# Essa função seleciona aleatoriamente uma musica da lista de gostos do usuário.
# Também cria uma lista de itens para possiveis respostas. Excluindo a propria resposta.
# parametro field é chave de busca no objeto.
def get_basic_music_props(field, user_likes_music):
    music = user_likes_music[random.randint(0, len(user_likes_music)-1)]
    range_items = random.randint(0, len(artists)-4)
    items = artists[range_items:range_items+3]

    final_items = []
    for item in items:
        if music[field] == item[field]:
            final_items.append(
                artists[(range_items-1)if range_items >= 1 else (range_items+1)][field])
        else:
            final_items.append(item[field])

    return (music, final_items)


# mesma funcionalidade da basic_music_props. field define o que vc quer do objeto movie.
def get_basic_movie_props(field, user_likes_movie):
    movie = user_likes_movie[random.randint(0, len(user_likes_movie)-1)]
    range_items = random.randint(0, len(movies)-4)
    items = movies[range_items:range_items+3]

    final_items = []
    for item in items:
        if movie[field] == item[field]:
            final_items.append(
                movies[(range_items-1)if range_items >= 1 else (range_items+1)][field])
        else:
            final_items.append(item[field])

    return (movie, final_items)


# informação dos amigos.
# isso não está completamente testado... Talvez tenha que analisar melhor como generalizar essa função, esse caso é mais específico.
def get_basic_friend_props(field):
    friend = mining[random.randint(0, len(mining)-1)]
    range_items = random.randint(0, len(persons)-4)
    items = persons[range_items:range_items+3]

    final_items = []
    for item in items:
        if friend[field] == item[field]:
            final_items.append(
                persons[(range_items-1)if range_items >= 1 else (range_items+1)][field])
        else:
            final_items.append(item[field])

    return (friend, final_items)


def get_advanced_friend_props(field):
    pass


# Musicas
def music_question_img(user_likes_music):
    music, final_items = get_basic_music_props("name", user_likes_music)

    music_name = music["name"]
    request_url = f"""https://api.giphy.com/v1/gifs/search?api_key=2BlX1Laj1leZ36dEoyKKaqVlWgj85mjq&q={music_name}&limit=25&offset=0&rating=G&lang=en"""

    response = requests.get(request_url)
    data = response.json()

    ms_img = data["data"][0]["images"]["downsized_large"]["url"]

    return {
        "question_type": 0,
        "question": "De qual banda é essa imagem?",
        "image": ms_img,
        "answer": music["name"],
        "items": final_items}


def music_question_logo(user_likes_music):
    music, final_items = get_basic_music_props("name", user_likes_music)

    giphy_images = []
    for name in final_items:
        loc = f"""https://api.giphy.com/v1/gifs/search?api_key=2BlX1Laj1leZ36dEoyKKaqVlWgj85mjq&q={name}&limit=25&offset=0&rating=G&lang=en"""
        response = requests.get(loc)
        data = response.json()
        giphy_images.append(data["data"][0]["images"]
                            ["downsized_large"]["url"])

    music_name = music["name"]
    request_url = f"""https://api.giphy.com/v1/gifs/search?api_key=2BlX1Laj1leZ36dEoyKKaqVlWgj85mjq&q={music_name}&limit=25&offset=0&rating=G&lang=en"""

    response = requests.get(request_url)
    data = response.json()

    ms_img = data["data"][0]["images"]["downsized_large"]["url"]

    artist_name = music["name"]
    return {
        "question_type": 1,
        "question": f"Qual imagem identifica a banda: {artist_name}?",
        "answer": ms_img,
        "items": giphy_images}


def music_question_genre(user_likes_music):
    music, final_items = get_basic_music_props("genres", user_likes_music)

    final_items_string = []
    for item in final_items:
        final_items_string.append(item[0])

    artist_name = music["name"]
    return {
        "question_type": 2,
        "question": f"Qual conjunto de generos a banda {artist_name} se encaixa?",
        "answer": music["genres"][0],
        "items": final_items_string}


# Filmes
def movie_question_img(user_likes_movie):
    # Isso ta meio bugado. Image retorna o próprio cartaz do filme... Acho que seria legal, pegar a ideia de que essa imagem tem que ser um GIF do giphy, assim fica mais legal de adivinhar. Por enquanto, vai ficar assim.
    # https://api.giphy.com/v1/gifs/search?api_key=2BlX1Laj1leZ36dEoyKKaqVlWgj85mjq&q=Hobbit&limit=25&offset=0&rating=G&lang=en

    movie, final_items = get_basic_movie_props("title", user_likes_movie)

    movie_name = movie["title"]
    request_url = f"""https://api.giphy.com/v1/gifs/search?api_key=2BlX1Laj1leZ36dEoyKKaqVlWgj85mjq&q={movie_name}&limit=25&offset=0&rating=G&lang=en"""

    response = requests.get(request_url)
    data = response.json()

    mv_img = data["data"][0]["images"]["downsized_large"]["url"]

    return {
        "question_type": 3,
        "question": "De qual filme é essa cena?",
        "image": mv_img,
        "answer": movie["title"],
        "items": final_items}


def movie_question_scene(user_likes_movie):
    # Isso ta meio bugado. Image retorna o próprio cartaz do filme... Acho que seria legal, pegar a ideia de que essa imagem tem que ser um GIF do giphy, assim fica mais legal de adivinhar. Por enquanto, vai ficar assim.

    movie, final_items = get_basic_movie_props("title", user_likes_movie)

    giphy_images = []
    for name in final_items:
        loc = f"""https://api.giphy.com/v1/gifs/search?api_key=2BlX1Laj1leZ36dEoyKKaqVlWgj85mjq&q={name}&limit=25&offset=0&rating=G&lang=en"""
        response = requests.get(loc)
        data = response.json()
        giphy_images.append(data["data"][0]["images"]
                            ["downsized_large"]["url"])

    movie_name = movie["title"]
    request_url = f"""https://api.giphy.com/v1/gifs/search?api_key=2BlX1Laj1leZ36dEoyKKaqVlWgj85mjq&q={movie_name}&limit=25&offset=0&rating=G&lang=en"""

    response = requests.get(request_url)
    data = response.json()

    mv_img = data["data"][0]["images"]["downsized_large"]["url"]
    movie_name = movie["title"]

    return {
        "question_type": 4,
        "question": f"Qual cena pertence ao filme: {movie_name}?",
        "answer": mv_img,
        "items": giphy_images}


def movie_question_director(user_likes_movie):
    # Isso ta meio bugado. Image retorna o próprio cartaz do filme... Acho que seria legal, pegar a ideia de que essa imagem tem que ser um GIF do giphy, assim fica mais legal de adivinhar. Por enquanto, vai ficar assim.

    movie, final_items = get_basic_movie_props("director", user_likes_movie)

    movie_name = movie["title"]

    return {
        "question_type": 5,
        "question": f"Qual o diretor do filme: {movie_name}?",
        "answer": movie["director"],
        "items": final_items}
# Friends


# pergunta idiota com o único intuito de testar e deixar um modelo pronto. Praticamente todos nasceram em Curitiba então a maioria do "final_items" vai ter items de curitiba.
def frieds_question_birthdate():
    # user objeto global "mining" aqui
    friend, final_items = get_basic_friend_props("birthdate")

    friend_name = friend["name"]
    return {
        "question_type": 0,
        "question": f"Qual a data de nascimento do seu amigo(a): {friend_name}?",
        "answer": friend["birthdate"],
        "items": final_items}


def frieds_question_movie():
    # user objeto global "mining" aqui
    friend, final_items = get_basic_friend_props("birthdate")

    friend_name = friend["name"]
    return {
        "question_type": 0,
        "question": f"Qual a data de nascimento do seu amigo(a): {friend_name}?",
        "answer": friend["birthdate"],
        "items": final_items}


# retorna array com as questões para musicas
def get_music_questions():
    music_questions = [music_question_img,
                       music_question_logo, music_question_genre]
    return music_questions


# cada nova função para as questões de filme, devem ser adicionadas aqui, no vetor movie_questions.
def get_movie_questions():
    movie_questions = [movie_question_img,
                       movie_question_scene, movie_question_director]
    return movie_questions


# cada nova função para as questões de amigos, devem ser adicionadas aqui, no vetor friends_questions.
def get_friends_questions():
    friends_questions = [frieds_question_birthdate]
    return friends_questions


def populate_general_information(_artists, _movies, _mining, _persons):
    global mining
    global artists
    global movies
    global persons
    persons = list(_persons)
    mining = _mining
    artists = list(_artists)
    movies = list(_movies)


# Abaixo, um exemplo do que cada objeto contém, para facilitar alterações.
#
# user_likes_music = {
#    _id: ObjectId("5de59b7f14...e169fa3d"),
#    genres: ["j-pop", "j-rock", "visual kei"],
#    id: "https://en.wikipedi...e_Ok_Rock",
#    image: "https://i.scdn.co/i...b14e646a6",
#    name: "ONE OK ROCK",
#    popularity: 74
# };

# user_likes_movie = [
#    {
#        _id: ObjectId("5de59b8714...e169fb66"),
#        actors: "Jake Gyllenhaal, Ho...igh Chase",
#        country: "USA",
#        director: "Richard Kelly",
#        genre: "Drama, Sci-Fi, Thriller",
#        id: "tt0246578",
#        image: "https://m.media-ama...SX300.jpg",
#        title: "Donnie Darko",
#        year: "2001"
#    }
# ];

# mining = [
#    {
#        likes_movie: [
#            {
#                _id: ObjectId("5de59b8714...e169fb66"),
#                actors: "Jake Gyllenhaal, Ho...igh Chase",
#                country: "USA",
#                director: "Richard Kelly",
#                genre: "Drama, Sci-Fi, Thriller",
#                id: "tt0246578",
#                image: "https://m.media-ama...SX300.jpg",
#                title: "Donnie Darko",
#                year: "2001"
#            }
#        ],
#        likes_music: [
#            {
#                _id: ObjectId("5de59b7f14...e169fa3d"),
#                genres: ["j-pop", "j-rock", "visual kei"],
#                id: "https://en.wikipedi...e_Ok_Rock",
#                image: "https://i.scdn.co/i...b14e646a6",
#                name: "ONE OK ROCK",
#                popularity: 74
#            }
#        ],
#        name: "Iuri Igarashi",
#        knows: [
#            "Anderson Candido De... Da Silva",
#            "Victor Hugo Silva D...s Gabriel",
#            "Vitor Da Costa Mamede Correa"
#        ],
#        hometown: "Curitiba",
#        birthdate: 01/05/1995,
#    }
# ];
