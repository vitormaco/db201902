import urllib.request
import xml.etree.ElementTree as ET
import psycopg2

def executeInsertQuery(cursor, table, values):
  query_string = 'INSERT INTO ' + table + ' VALUES(%s' + (', %s' * (len(values)-1)) + ')'
  cursor.execute(query_string, (values))

connection = psycopg2.connect(host='200.134.10.32', database='201902PowerPuff_DBgirls', user='201902PowerPuff_DBgirls', password='300005')
cursor = connection.cursor()

dainf_xml_url = 'http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data'

cursor.execute('delete from knows')
cursor.execute('delete from likesmovie')
cursor.execute('delete from likesmusic')
cursor.execute('delete from person')
print('todo o conteudo das tabelas foi apagado')

contents = urllib.request.urlopen(dainf_xml_url + '/person.xml').read()
tree = ET.ElementTree(ET.fromstring(contents))
for child in tree.getroot():
  elem = child.attrib
  insert_values = [elem['uri'], elem['name'], elem['hometown'], elem['birthdate'] if elem['birthdate'] else None]
  executeInsertQuery(cursor, 'person', insert_values)
print('conteudo inserido na tabela person')

contents = urllib.request.urlopen(dainf_xml_url + '/music.xml').read()
tree = ET.ElementTree(ET.fromstring(contents))
for child in tree.getroot():
  elem = child.attrib
  insert_values = [elem['person'], elem['rating'], elem['bandUri']]
  executeInsertQuery(cursor, 'likesmusic', insert_values)
print('conteudo inserido na tabela likesmusic')

contents = urllib.request.urlopen(dainf_xml_url + '/movie.xml').read()
tree = ET.ElementTree(ET.fromstring(contents))
for child in tree.getroot():
  elem = child.attrib
  insert_values = [elem['person'], elem['rating'], elem['movieUri']]
  # tratamento de uma entrada duplicada no xml do professor
  if elem['person'] == 'http://utfpr.edu.br/CSB30/2019/2/DI1902alexandrebonacim' \
    and elem['rating'] == '4' \
    and elem['movieUri'] == 'http://www.imdb.com/title/tt0167260/':
    continue
  executeInsertQuery(cursor, 'likesmovie', insert_values)
print('conteudo inserido na tabela likesmovie')

contents = urllib.request.urlopen(dainf_xml_url + '/knows.xml').read()
tree = ET.ElementTree(ET.fromstring(contents))
for child in tree.getroot():
  elem = child.attrib
  insert_values = [elem['person'], elem['colleague']]
  executeInsertQuery(cursor, 'knows', insert_values)
print('conteudo inserido na tabela knows')

connection.commit()
cursor.close()
connection.close()
