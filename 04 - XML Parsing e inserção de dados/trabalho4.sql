CREATE TABLE Person(
	uri VARCHAR(100) NOT NULL,
	name VARCHAR(100) NOT NULL,
	hometown VARCHAR(100),
	birthdate DATE,
	PRIMARY KEY(uri)
);

CREATE TABLE LikesMusic(
	person VARCHAR(100) NOT NULL,
	rating INTEGER NOT NULL,
	bandUri VARCHAR(100) NOT NULL,
	PRIMARY KEY(person, bandUri),
	FOREIGN KEY(person)
		REFERENCES Person(uri)
);

CREATE TABLE LikesMovie(
	person VARCHAR(100) NOT NULL,
	rating INTEGER NOT NULL,
	movieUri VARCHAR(100) NOT NULL,
	PRIMARY KEY(person, movieUri),
	FOREIGN KEY(person)
		REFERENCES Person(uri)
);

CREATE TABLE Knows(
	person VARCHAR(100) NOT NULL,
	colleague VARCHAR(100) NOT NULL,
	PRIMARY KEY(person, colleague),
	FOREIGN KEY(person)
		REFERENCES Person(uri),
	FOREIGN KEY(colleague)
		REFERENCES Person(uri)
);
