--Tipo Entidade:

CREATE TABLE Usuario(
	login VARCHAR(30) NOT NULL,
	cidade VARCHAR(30) NOT NULL,
	nome_completo VARCHAR(30) NOT NULL,
	PRIMARY KEY(login)
);

CREATE TABLE ArtistaMusical(
	id INTEGER NOT NULL,
	nome_artistico VARCHAR(30) NOT NULL,
	genero_musical VARCHAR(30) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE Cantor(
	id INTEGER NOT NULL,
	nome_real VARCHAR(30) NOT NULL,
	data_de_nascimento DATE NOT NULL,
	estilo_musical VARCHAR(30) NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(id)
		REFERENCES ArtistaMusical(id)
);

CREATE TABLE Grupo(
	id INTEGER NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(id)
		REFERENCES ArtistaMusical(id)
);

CREATE TABLE Musico(
	nome_real VARCHAR(30) NOT NULL,
	data_de_nascimento DATE NOT NULL,
	estilo_musical VARCHAR(30) NOT NULL,
	PRIMARY KEY(nome_real)
);

CREATE TABLE Equipe(
	id INTEGER NOT NULL,
	telefone VARCHAR(30) NOT NULL,
	endereco VARCHAR(100) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE Ator(
	id INTEGER NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(id)
		REFERENCES Equipe(id)
);

CREATE TABLE Diretor(
	id INTEGER NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(id)
		REFERENCES Equipe(id)
);

CREATE TABLE Filme(
	id INTEGER NOT NULL,
	nome VARCHAR(30) NOT NULL,
	data_de_lancamento DATE NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE Dupla(
	id INTEGER NOT NULL,
	musico1 VARCHAR(30) NOT NULL,
	musico2 VARCHAR(30) NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY(id)
		REFERENCES ArtistaMusical(id),
	FOREIGN KEY(musico1)
		REFERENCES Musico(nome_real),
	FOREIGN KEY(musico2)
		REFERENCES Musico(nome_real)
);

CREATE TABLE Categoria(
	nome VARCHAR(30) NOT NULL,
	super VARCHAR(30),
	PRIMARY KEY(nome),
	FOREIGN KEY(super)
		REFERENCES Categoria(nome)
);

--Relações:

CREATE TABLE Participa(
	id_ator INTEGER NOT NULL,
	id_filme INTEGER NOT NULL,
	salario FLOAT NOT NULL,
	PRIMARY KEY(id_ator, id_filme),
	FOREIGN KEY(id_ator)
		REFERENCES Ator(id),
	FOREIGN KEY(id_filme)
		REFERENCES Filme(id)
);

CREATE TABLE Dirige(
	id_diretor INTEGER NOT NULL,
	id_filme INTEGER NOT NULL,
	salario FLOAT NOT NULL,
	PRIMARY KEY(id_diretor, id_filme),
	FOREIGN KEY(id_diretor)
		REFERENCES Diretor(id),
	FOREIGN KEY(id_filme)
		REFERENCES Filme(id)
);

CREATE TABLE Conhece(
	usuario1 VARCHAR(30) NOT NULL,
	usuario2 VARCHAR(30) NOT NULL,
	PRIMARY KEY(usuario1, usuario2),
	FOREIGN KEY(usuario1)
		REFERENCES Usuario(login),
	FOREIGN KEY(usuario2)
		REFERENCES Usuario(login)
);

CREATE TABLE Bloqueio(
	usuario1 VARCHAR(30) NOT NULL,
	usuario2 VARCHAR(30) NOT NULL,
	razao VARCHAR(100),
	PRIMARY KEY(usuario1, usuario2),
	FOREIGN KEY(usuario1)
		REFERENCES Usuario(login),
	FOREIGN KEY(usuario2)
		REFERENCES Usuario(login)
);

CREATE TABLE CurteFilme(
	usuario VARCHAR(30) NOT NULL,
	filme INTEGER NOT NULL,
	nota INTEGER NOT NULL,
	PRIMARY KEY(usuario, filme),
	FOREIGN KEY(usuario)
		REFERENCES Usuario(login),
	FOREIGN KEY(filme)
		REFERENCES Filme(id)
);

CREATE TABLE CurteArtista(
	usuario VARCHAR(30) NOT NULL,
	artista INTEGER NOT NULL,
	nota INTEGER NOT NULL,
	PRIMARY KEY(usuario, artista),
	FOREIGN KEY(usuario)
		REFERENCES Usuario(login),
	FOREIGN KEY(artista)
		REFERENCES ArtistaMusical(id)
);

CREATE TABLE GruposPossuem(
	grupo INTEGER NOT NULL,
	musico VARCHAR(30) NOT NULL,
	PRIMARY KEY(grupo, musico),
	FOREIGN KEY(grupo)
		REFERENCES Grupo(id),
	FOREIGN KEY(musico)
		REFERENCES Musico(nome_real)
);

CREATE TABLE Contem(
	filme INTEGER NOT NULL,
	categoria VARCHAR(30) NOT NULL,
	PRIMARY KEY(filme, categoria),
	FOREIGN KEY(filme)
		REFERENCES Filme(id),
	FOREIGN KEY(categoria)
		REFERENCES Categoria(nome)
);

-- Inserts Teste:
-- Usuario
INSERT INTO Usuario VALUES('xadu', 'curitiba', 'iuri igarashi');
INSERT INTO Usuario VALUES('zem', 'curitiba', 'felipe lopes zem');
INSERT INTO Usuario VALUES('vitormaco', 'curitiba', 'vitor da costa');
INSERT INTO Usuario VALUES('anderson', 'curitiba', 'anderson da silva');
INSERT INTO Usuario VALUES('Habib', 'Joinville', 'Jorge Habib');
-- ArtistaMusical
INSERT INTO ArtistaMusical VALUES(1, 'Massacration', 'Metal');
INSERT INTO ArtistaMusical VALUES(2, 'Rammstein', 'Metal');
INSERT INTO ArtistaMusical VALUES(3, 'Queen', 'Rock');
INSERT INTO ArtistaMusical VALUES(4, 'The Beatles', 'Rock');
INSERT INTO ArtistaMusical VALUES(5, 'Freddie Mercury', 'Rock');
INSERT INTO ArtistaMusical VALUES(6, 'Lady Gaga', 'Pop');
INSERT INTO ArtistaMusical VALUES(7, 'Simon e Garfunkel', 'Folk');
INSERT INTO ArtistaMusical VALUES(8, 'The White Stripes', 'Rock');
--Cantor
INSERT INTO Cantor VALUES(5, 'Farrokh Bulsara', '1946-09-05', 'Rock');
INSERT INTO Cantor VALUES(6, 'Stefani Joanne', '1986-04-28', 'Pop');
INSERT INTO Cantor VALUES(4, 'John Wiston Lennon', '1980-12-08', 'Rock');
INSERT INTO Cantor VALUES(3, 'Freddie Mercury', '1935-04-01', 'Rock');
-- Grupo
INSERT INTO Grupo VALUES(1);
INSERT INTO Grupo VALUES(2);
INSERT INTO Grupo VALUES(3);
INSERT INTO Grupo VALUES(4);
-- Músico
INSERT INTO Musico VALUES('Farrokh Bulsara', '1946-09-05', 'Rock');
INSERT INTO Musico VALUES('Stefani Joanne', '1986-04-28', 'Pop');
INSERT INTO Musico VALUES('John Wiston Lennon', '1980-12-08', 'Rock');
INSERT INTO Musico VALUES('George Harrison', '1943-02-25', 'Rock');
INSERT INTO Musico VALUES('James Paul McCartney', '1942-06-18', 'Rock');
INSERT INTO Musico VALUES('Richard Starkey', '1940-07-07', 'Rock');
INSERT INTO Musico VALUES('Simon', '1940-07-07', 'Folk');
INSERT INTO Musico VALUES('Garfunkel', '1940-07-07', 'Folk');
INSERT INTO Musico VALUES('Jack White', '1940-07-07', 'Rock');
INSERT INTO Musico VALUES('Meg White', '1940-07-07', 'Rock');
-- Equipe
INSERT INTO Equipe VALUES(1,'3145-5523','Rua da Saudade');
INSERT INTO Equipe VALUES(2,'3325-5533','Rua da Paixão');
INSERT INTO Equipe VALUES(3,'3023-3333','Rua da Programação');
INSERT INTO Equipe VALUES(4,'3731-1913','Rua da Lua');
INSERT INTO Equipe VALUES(5,'3232-1113','Rua da Baguncinha');
INSERT INTO Equipe VALUES(6,'3111-2334','Av Brasil');
INSERT INTO Equipe VALUES(7,'3731-1456','Rua da Xuxa');
INSERT INTO Equipe VALUES(8,'5445-5663','Rua da Lula');
INSERT INTO Equipe VALUES(9,'3234-1424','PF');
-- Ator
INSERT INTO Ator VALUES(1);
INSERT INTO Ator VALUES(2);
INSERT INTO Ator VALUES(3);
INSERT INTO Ator VALUES(7);
INSERT INTO Ator VALUES(8);
INSERT INTO Ator VALUES(9);
-- Diretor
INSERT INTO Diretor VALUES(4);
INSERT INTO Diretor VALUES(5);
INSERT INTO Diretor VALUES(6);
-- Filme
INSERT INTO Filme VALUES(1,'V de Vingança','2000-02-03');
INSERT INTO Filme VALUES(2,'Harry Potter','2004-02-03');
INSERT INTO Filme VALUES(3,'Winx','2008-02-03');
INSERT INTO Filme VALUES(4,'Simpsons','2010-02-03');
INSERT INTO Filme VALUES(5,'Kill Bill','2001-02-03');
INSERT INTO Filme VALUES(6,'Mad Max','2017-02-03');
--Dupla
INSERT INTO Dupla VALUES(4,'John Wiston Lennon','James Paul McCartney');
INSERT INTO Dupla VALUES(7, 'Simon', 'Garfunkel');
INSERT INTO Dupla VALUES(8, 'Jack White', 'Meg White');
-- Participa
INSERT INTO Participa VALUES(1,1,15000);
INSERT INTO Participa VALUES(2,2,30000);
INSERT INTO Participa VALUES(3,3,99000);
INSERT INTO Participa VALUES(7,4,4453);
INSERT INTO Participa VALUES(8,5,12565);
INSERT INTO Participa VALUES(9,6,9822);
-- Dirige
INSERT INTO Dirige VALUES(4,1,25000);
INSERT INTO Dirige VALUES(5,2,40000);
INSERT INTO Dirige VALUES(6,3,25000);
-- Conhece
INSERT INTO Conhece VALUES('xadu','zem');
INSERT INTO Conhece VALUES('xadu','vitormaco');
INSERT INTO Conhece VALUES('xadu','anderson');
INSERT INTO Conhece VALUES('xadu','Habib');
INSERT INTO Conhece VALUES('anderson','vitormaco');
INSERT INTO Conhece VALUES('anderson','zem');
INSERT INTO Conhece VALUES('anderson','Habib');
-- Bloqueio
INSERT INTO Bloqueio VALUES('vitormaco','xadu','Eu quis');
INSERT INTO Bloqueio VALUES('vitormaco','zem');
INSERT INTO Bloqueio VALUES('vitormaco','anderson');
INSERT INTO Bloqueio VALUES('Habib','vitormaco');
INSERT INTO Bloqueio VALUES('Habib','zem');
INSERT INTO Bloqueio VALUES('anderson','Habib');
-- CurteFilme
INSERT INTO CurteFilme VALUES('vitormaco',1,5);
INSERT INTO CurteFilme VALUES('Habib',2,10);
INSERT INTO CurteFilme VALUES('anderson',3,10);
INSERT INTO CurteFilme VALUES('vitormaco',4,5);
INSERT INTO CurteFilme VALUES('Habib',5,3);
INSERT INTO CurteFilme VALUES('anderson',6,7);
-- CurteArtista
INSERT INTO CurteArtista VALUES('vitormaco',4,5);
INSERT INTO CurteArtista VALUES('Habib',5,10);
INSERT INTO CurteArtista VALUES('xadu',6,7);
INSERT INTO CurteArtista VALUES('zem',1,6);
INSERT INTO CurteArtista VALUES('Habib',2,8);
INSERT INTO CurteArtista VALUES('anderson',8,1);
-- Categoria
INSERT INTO Categoria VALUES('Terror');
INSERT INTO Categoria VALUES('Suspense');
INSERT INTO Categoria VALUES('Drama');
INSERT INTO Categoria VALUES('Ação');
INSERT INTO Categoria VALUES('Comédia');
INSERT INTO Categoria VALUES('Comédia Romantica','Comédia');
-- GruposPossuem
INSERT INTO GruposPossuem VALUES(1,'Meg White');
INSERT INTO GruposPossuem VALUES(2,'Simon');
INSERT INTO GruposPossuem VALUES(3,'Garfunkel');
-- Contem
INSERT INTO Contem VALUES(1,'Suspense');
INSERT INTO Contem VALUES(2,'Ação');
INSERT INTO Contem VALUES(3,'Comédia');
INSERT INTO Contem VALUES(4,'Comédia');
INSERT INTO Contem VALUES(5,'Ação');
INSERT INTO Contem VALUES(6,'Drama');