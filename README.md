### Vitor Corrêa - RA: 1905112 - Login: vitormaco

### Iuri Igarashi - RA: 1100629 - Login: iuriigarashi

### Anderson - RA: 1986740 - Login: rul3rst4

![Screenshot](ppg.jpg)

<br />
<br />

# Questionário do trabalho final:

### - O que vocês vão implementar

Um jogo de perguntas e respostas visando utilizar e coletar dados.

### - Quais tecnologias vão utilizar

HTML, CSS, javascript, Python, SQL, NoSQL, React

### - Por que decidiram pelo tema

Achamos ser um tema desafiador que permite ao mesmo tempo utilizar os dados que já temos e coletar outros para aprimorar nossa plataforma.

### - Quais os desafios relacionados com bancos de dados (consultas, modelos, etc.)

Precisamos criar uma rede de relacionamentos para conectar cada usuário da rede social e modelar como nossos dados serão armazenados. As consultas serão complexas pois envolverão várias tabelas para poder relacionar os dados e encontrar informações relevantes.

### - Quais os desafios relacionados com suas áreas de interesse

### - Quais conhecimentos novos vocês pretendem adquirir

### - O que vocês já produziram (protótipos, esquemas de telas, teste de codificação de partes críticas, etc.)

Um protótipo do aplicativo com as principais telas: Login, Profile, Question, GameMode

<br />
<br />

# Questionário Individual:

### - Nome:

Iuri Igarashi

### - Qual o seu foco na implementação do trabalho?

BackEnd, FrontEnd e SQL

### - Já teve alguma experiência com as tecnologias que serão usadas no trabalho? Quais?

React

### - Qual aspecto do trabalho te interessa mais?

Aprender mais sobre a integração do backend com o frontend.

<br />
<br />
<br />
<br />

### - Nome:

Vitor

### - Qual o seu foco na implementação do trabalho?

BackEnd, FrontEnd e SQL

### - Já teve alguma experiência com as tecnologias que serão usadas no trabalho? Quais?

Sim. Apenas não tenho experiencia em NoSQL.

### - Qual aspecto do trabalho te interessa mais?

Algoritmos de recomendação e data mining.

<br />
<br />
<br />
<br />

### - Nome:

Anderson

### - Qual o seu foco na implementação do trabalho?

BackEnd, FrontEnd e SQL

### - Já teve alguma experiência com as tecnologias que serão usadas no trabalho? Quais?

Sim. Apenas não tenho experiencia em NoSQL.

### - Qual aspecto do trabalho te interessa mais?

Algoritmos de data mining, utilização de Python e análise dos dados.

<br />
<br />
<br />
<br />
