const express = require("express");
require("dotenv").config();
const { Pool, Client } = require("pg");
const cors = require("cors");
const app = express();
const axios = require("axios");
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(express.static(`./public`));

const client = new Client();
client.connect();

app.get("/peopleXmovie", (req, res) => {
    client
        .query(
            `
      WITH contagem as
      (SELECT person, count(*) as votos
      FROM LikesMovie lm
      JOIN Person p on p.uri=lm.person
      group by person)
      SELECT votos, count(*) as pessoa
      FROM contagem
      group by votos;

      ;
    `
        )
        .then(result => {
            res.send(result.rows);
        });
});

app.get("/conhecidos_likesmovie", (req, res) => {
    client
        .query(
            `
			SELECT * FROM
			(SELECT qz.person, qz.conhecido, count(*)
			FROM
			(SELECT * FROM
			ConheceNormalizada CN, Likesmovie LM
			WHERE LM.person = CN.pessoa
			AND CN.conhecido IN (SELECT lsm.person from Likesmovie lsm where lsm.person=CN.conhecido AND LM.movieuri=lsm.movieuri)) as qz
			GROUP BY qz.person, qz.conhecido) as magicQuery
			where magicQuery.count = (
			SELECT max(count)
			FROM (SELECT qz.person, qz.conhecido, count(*)
			FROM
			(SELECT * FROM
			ConheceNormalizada CN, Likesmovie LM
			WHERE LM.person = CN.pessoa
			AND CN.conhecido IN (SELECT lsm.person from Likesmovie lsm where lsm.person=CN.conhecido AND LM.movieuri=lsm.movieuri)) as qz
			GROUP BY qz.person, qz.conhecido) as innQuery where innQuery.person = magicQuery.person
			)
			;
		`
        )
        .then(result => {
            res.send(result.rows);
        })
        .catch(err => console.log(err));
});

/*
	Query para criar ConheceNormalizada:

	CREATE VIEW ConheceNormalizada AS

	(
		SELECT c1.person as person, c1.colleague as colleague
		FROM Knows c1
		UNION
		SELECT c2.colleague as person, c2.person as colleague
		FROM Knows c2
	);
*/
app.get("/conheceNormalizada", (req, res) => {
    client
        .query(
            `
      select * from ConheceNormalizada
    `
        )
        .then(result => {
            res.send(result.rows);
        })
        .catch(err => res.send(err));
});

app.get("/conhecidosPowerPuff", (req, res) => {
    client
        .query(
            `
      SELECT pessoa,count(*)
      from ConheceNormalizada
      where pessoa IN ('http://utfpr.edu.br/CSB30/2019/2/DI1902andersonsilva','http://utfpr.edu.br/CSB30/2019/2/DI1902vitorcorrea','http://utfpr.edu.br/CSB30/2019/2/DI1902iuriigarashi')
      group by pessoa
    `
        )
        .then(result => {
            res.send(result.rows);
        })
        .catch(err => res.send(err));
});

app.get("/movieXpeople", (req, res) => {
    client
        .query(
            `
      WITH contagem as
      (SELECT movieuri, count(*) votos
      FROM LikesMovie lm
      JOIN Person p on p.uri=lm.person
      group by movieuri)
      SELECT votos, count(*) as contador
      from contagem
      group by votos
      ;
    `
        )
        .then(result => {
            res.send(result.rows);
        });
});

app.get("/movieAPI", (req, res) => {
    client
        .query(
            `
      select distinct movieuri from likesmovie;
    `
        )
        .then(result1 => {
            let IMDBcontainer = [];
            let parsedData = [];
            result1.rows.forEach(element => {
                let movieId = element.movieuri.split("/");
                IMDBcontainer.push(movieId[movieId.length - 2]);
            });
            let finalResult = [];
            Promise.all(
                IMDBcontainer.map(element =>
                    axios.get(
                        `http://www.omdbapi.com/?i=${element}&apikey=da9ff8a5`
                    )
                )
            ).then(result => {
                finalResult = result.map(element => {
                    return element.data;
                });
            });

            let promisseArray = [];
            let timeCount = 7000;
            (async function requests() {
                for (let index = 0; index < IMDBcontainer.length; index++) {
                    if ((index + 1) % 20 == 0) {
                        await new Promise(resolve => {
                            setTimeout(resolve, timeCount);
                        });
                    }

                    promisseArray.push(
                        axios.get(
                            `https://api.themoviedb.org/3/movie/${IMDBcontainer[index]}?api_key=3f75efc6ea35e6ab571210052a4fe508&language=en-US`
                        )
                    );
                }
                Promise.all(promisseArray)
                    .then(result => {
                        let data = result.map(element => {
                            return element.data;
                        });
                        for (let i = 0; i < finalResult.length; i++) {
                            finalResult[i].budget = data[i].budget;
                            finalResult[i].revenue = data[i].revenue;
                        }
                        res.send(finalResult);
                    })
                    .catch(err => {
                        console.log(err);
                    });
            })();
        })
        .catch(err => {
            console.log(err);
        });
});

app.get("/music", (req, res) => {
    client
        .query(
            `
      SELECT * FROM likesmusic;
    `
        )
        .then(result => {
            res.send(result.rows);
        });
});

app.get("/movie", (req, res) => {
    client
        .query(
            `
      SELECT * FROM likesmovie;
    `
        )
        .then(result => {
            res.send(result.rows);
        });
});

app.get("/musicAVGatLeastTwo", (req, res) => {
    client
        .query(
            `
      select banduri, avg(rating) from likesmusic
      group by banduri
      having count(*) >=2
      order by avg(rating) DESC
    `
        )
        .then(result => {
            res.send(result.rows);
        });
});

app.get("/movieAVGatLeastTwo", (req, res) => {
    client
        .query(
            `
      select movieuri, avg(rating) from likesmovie
      group by movieuri
      having count(*) >=2
      order by avg(rating) DESC
    `
        )
        .then(result => {
            res.send(result.rows);
        });
});

app.get("/filmeAbaixoDaMedia", (req, res) => {
    client
        .query(
            `
      select movieuri,avg(rating) from likesmovie
      group by movieuri
      having avg(rating) <= (SELECT avg(rating) from likesmovie)
      order by avg(rating)
    `
        )
        .then(result => {
            res.send(result.rows);
        });
});

app.get("/musicaAbaixoDaMedia", (req, res) => {
    client
        .query(
            `
      select banduri,avg(rating) from likesmusic
      group by banduri
      having avg(rating) <= (SELECT avg(rating) from likesmusic)
      order by avg(rating)
    `
        )
        .then(result => {
            res.send(result.rows);
        });
});

app.post("/saveMovieXML", (req, res) => {
    console.log(req.body);

    res.send("OK");
});

app.post("/saveMusicXML", (req, res) => {
    console.log(req.body);

    res.send("OK");
});

const server = app.listen(3333, () =>
    console.log(`Running at port ${server.address().port}`)
);
