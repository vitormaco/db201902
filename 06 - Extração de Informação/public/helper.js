function getContext() {
    // Create and return canvas.
    const div = document.createElement("div");
    const ctx = document.createElement("canvas");
    div.style = "max-width: 800px;";
    main.appendChild(div);
    ctx.width = 800;
    ctx.height = 400;
    div.appendChild(ctx);
    return ctx;
}

function createListElement(text) {
    const li = document.createElement("li");
    li.textContent = text;
    return li;
}

function createChart(header, data, chart) {
    const myChart = new Chart(chart, {
        type: "bar",
        data: {
            labels: header,
            datasets: [
                {
                    label: "# of Votes",
                    data: data,
                    backgroundColor: Chart.defaults.global.defaultColor,
                    borderWidth: 1,
                },
            ],
        },
        options: {
            scales: {
                yAxes: [
                    {
                        ticks: {
                            beginAtZero: true,
                        },
                    },
                ],
            },
        },
    });
}

function digestPage(artist, page) {
    const infobox = getInfoboxData(artist, page);
    if (!infobox) return;

    return {
        artist: artist,
        origin: parseOrigin(infobox),
        genre: parseGenre(infobox),
    };
}

function getInfoboxData(artist, doc) {
    if ((infobox = doc.getElementsByClassName("infobox vcard plainlist")).length != 0) {
        return infobox[0].childNodes[0];
    } else if ((infobox = doc.getElementsByClassName("infobox vcard biography")).length != 0) {
        return infobox[0].childNodes[0];
    } else {
        console.log("detalhes do artista " + artist + " nao encontrado");
    }
}

function parseOrigin(infobox) {
    let origin = null;
    infobox.childNodes.forEach(node => {
        if (
            node.childNodes[0] &&
            node.childNodes[0].childNodes[0] &&
            node.childNodes[0].childNodes[0].textContent == "Origin"
        ) {
            origin = node.childNodes[1].childNodes[0].textContent;
        }
        if (
            node.childNodes[0] &&
            node.childNodes[0].childNodes[0] &&
            node.childNodes[0].childNodes[0].textContent == "Born"
        ) {
            const country = node.childNodes[1].childNodes[node.childNodes[1].childNodes.length - 1].textContent;
            if (country.startsWith(",")) {
                origin = node.childNodes[1].childNodes[node.childNodes[1].childNodes.length - 2].textContent + country;
            } else {
                origin = country;
            }
        }
    });
    return origin;
}

function parseGenre(infobox) {
    let genre = null;
    infobox.childNodes.forEach(node => {
        if (
            node.childNodes[0] &&
            node.childNodes[0].childNodes[0] &&
            node.childNodes[0].childNodes[0].textContent == "Genres"
        ) {
            const raw_genres = node.childNodes[1].getElementsByTagName("a");
            let parsed_genres = [];
            for (let i = 0; i < raw_genres.length; i++) {
                parsed_genres.push(raw_genres[i].textContent);
            }
            genre = parsed_genres.join(", ").replace(/, \[[0-9]*\]/g, "");
        }
    });
    return genre;
}

// Standard deviation
let getSD = data => Math.sqrt(data.reduce((sq, n) => sq + Math.pow(n - getMean(data), 2), 0) / (data.length - 1));
// Arithmetic mean
let getMean = data => data.reduce((a, b) => (a >> 0) + (b >> 0)) / data.length;
