const URL = "http://localhost:3333";

lastfmEndpoint = "http://ws.audioscrobbler.com/2.0/";
lastfmApiKey = "api_key=5bc7811095f0137df272884f76e80839";

const ItemFunctionMap = {
    [mediaEdesvio]() {
        axios.get(URL + "/music").then(({ data }) => {
            const ratings = data.map(data => data.rating);
            const mean = getMean(ratings);
            const stdDev = getSD(ratings);

            let p = document.createElement("p");
            p.textContent = "Media das musicas: " + mean;
            main.appendChild(p);
            p = document.createElement("p");
            p.textContent = "Desvio padrao: " + stdDev;
            main.appendChild(p);
        });

        axios.get(URL + "/movie").then(({ data }) => {
            console.log(data);

            const ratings = data.map(data => data.rating);
            const mean = getMean(ratings);
            const stdDev = getSD(ratings);

            let p = document.createElement("p");
            p.textContent = "Media dos filmes: " + mean;
            main.appendChild(p);
            p = document.createElement("p");
            p.textContent = "Desvio padrao: " + stdDev;
            main.appendChild(p);
        });
    },
    [maiorRatingMedio]() {
        axios.get(URL + "/musicAVGatLeastTwo").then(({ data }) => {
            let h1 = document.createElement("h1");
            h1.textContent = "Artistas populares (Media)";
            main.appendChild(h1);
            data.forEach(element => {
                let p = document.createElement("p");
                p.textContent = element.banduri.split("wiki/")[1] + " -> " + element.avg;
                main.appendChild(p);
            });
        });

        axios.get(URL + "/movieAVGatLeastTwo").then(({ data }) => {
            let h1 = document.createElement("h1");
            h1.textContent = "Filmes populares (Media)";
            main.appendChild(h1);
            data.forEach(element => {
                let p = document.createElement("p");
                p.textContent = element.movieuri + " -> " + element.avg;
                main.appendChild(p);
            });
        });
    },
    [top10]() {
        axios.get(URL + "/music").then(({ data }) => {
            let popularArtists = {};
            data.forEach(element => {
                if (popularArtists[element["banduri"]]) {
                    popularArtists[element["banduri"]] += 1;
                } else {
                    popularArtists[element["banduri"]] = 1;
                }
            });
            sortedArtists = [];
            for (let movie in popularArtists) {
                sortedArtists.push([movie, popularArtists[movie]]);
            }
            sortedArtists = sortedArtists.sort((a, b) => b[1] - a[1]);

            let h1 = document.createElement("h1");
            h1.textContent = "Artistas mais populares";
            main.appendChild(h1);

            for (let i = 0; i < 10; i++) {
                let p = document.createElement("p");
                p.textContent = sortedArtists[i][0] + " -> " + sortedArtists[i][1];
                main.appendChild(p);
            }
        });

        axios.get(URL + "/movie").then(({ data }) => {
            let popularMovies = {};
            data.forEach(element => {
                if (popularMovies[element["movieuri"]]) {
                    popularMovies[element["movieuri"]] += 1;
                } else {
                    popularMovies[element["movieuri"]] = 1;
                }
            });
            sortedMovies = [];
            for (let movie in popularMovies) {
                sortedMovies.push([movie, popularMovies[movie]]);
            }
            sortedMovies = sortedMovies.sort((a, b) => b[1] - a[1]);

            let h1 = document.createElement("h1");
            h1.textContent = "Filmes mais populares";
            main.appendChild(h1);

            for (let i = 0; i < 10; i++) {
                let p = document.createElement("p");
                p.textContent = sortedMovies[i][0] + " -> " + sortedMovies[i][1];
                main.appendChild(p);
            }
        });
    },
    [relacionamentoConhecidos]() {
        axios.get(URL + "/conheceNormalizada").then(({ data }) => {
            data.forEach(element => {
                let p = document.createElement("p");
                p.textContent = element.pessoa.split("DI1902")[1] + " -> " + element.conhecido.split("DI1902")[1];
                main.appendChild(p);
            });
        });
    },
    [numeroDeConhecidos]() {
        axios.get(URL + "/conhecidosPowerPuff").then(({ data }) => {
            const arr = data.sort((a, b) => b.count - a.count);
            arr.forEach(element => {
                let p = document.createElement("p");
                p.textContent = element.pessoa.split("DI1902")[1] + " -> " + element.count;
                main.appendChild(p);
            });
        });
    },
    [conhecidosANDfilmes]() {
        axios.get(URL + "/conhecidos_likesmovie").then(({ data }) => {
            data.forEach(element => {
                let p = document.createElement("p");
                p.textContent =
                    element.person.split("DI1902")[1] +
                    " & " +
                    element.conhecido.split("DI1902")[1] +
                    " -> " +
                    element.count;
                main.appendChild(p);
            });
        });
    },
    [grafPessoaXFilme]() {
        axios.get(URL + "/peopleXmovie").then(({ data }) => {
            let headers = [];
            let apiData = [];

            data.forEach(element => {
                headers.push(element.pessoa);
                apiData.push(element.votos);
            });
            createChart(headers, apiData, getContext().getContext("2d"));
        });
    },
    [grafFilmeXPessoa]() {
        axios.get(URL + "/movieXpeople").then(({ data }) => {
            let headers = [];
            let apiData = [];

            data.forEach(element => {
                headers.push(element.votos);
                apiData.push(element.contador);
            });
            createChart(headers, apiData, getContext().getContext("2d"));
        });
    },
    [informacoesAdicionais]() {
        axios.get(URL + "/musicaAbaixoDaMedia").then(({ data }) => {
            let h1 = document.createElement("h1");
            h1.textContent = "Artistas não populares (Media)";
            main.appendChild(h1);
            data.forEach(element => {
                let p = document.createElement("p");
                p.textContent = element.banduri.split("wiki/")[1] + " -> " + element.avg;
                main.appendChild(p);
            });
        });

        axios.get(URL + "/filmeAbaixoDaMedia").then(({ data }) => {
            let h1 = document.createElement("h1");
            h1.textContent = "Filmes não populares (Media)";
            main.appendChild(h1);
            data.forEach(element => {
                let p = document.createElement("p");
                p.textContent = element.movieuri + " -> " + element.avg;
                main.appendChild(p);
            });
        });
    },
    [extracaoFilmes]() {
        alert("Aguarde um momento, essa query tem limite de requisição e vai demorar em média 1min...");
        axios.get(URL + "/movieAPI").then(({ data }) => {
            // Talvez depois de gerar a lista completa, dê para "postar" isso para o backend como plain HTML e gerar um xml com todas as tags que são inseridas aqui.
            const lista = document.createElement("ul");
            main.appendChild(lista);
            data.forEach(element => {
                const itemPrincipal = createListElement("Nome: " + element.Title);

                const listaInterna = document.createElement("ul");
                itemPrincipal.appendChild(listaInterna);

                listaInterna.appendChild(createListElement("Data de Lançamento: " + element.Released));
                listaInterna.appendChild(createListElement("Diretor: " + element.Director));
                listaInterna.appendChild(createListElement("Gênero: " + element.Genre));
                listaInterna.appendChild(createListElement("Despesa: " + element.budget));
                listaInterna.appendChild(createListElement("Receita: " + element.revenue));

                lista.appendChild(itemPrincipal);
            });

            axios.post(URL + "/saveMovieXML", { element: document.getElementById("main").innerHTML });
        });
    },
    [extracaoMusicas]() {
        axios.get(URL + "/music").then(({ data }) => {
            let artists = data.map(element => element["banduri"]);
            artists = [...new Set(artists)];
            artists = artists.map(url => url.split("/").pop());
            var parser = new DOMParser();
            for (let i = 0; i < artists.length; i++) {
                fetch(
                    `https://cors-anywhere.herokuapp.com/https://en.wikipedia.org/w/api.php?action=parse&page=${artists[i]}&format=json`
                )
                    .then(data => data.json())
                    .then(page => {
                        const artist = page["parse"]["title"];
                        const html = page["parse"]["text"]["*"];

                        const artist_name = artist
                            .replace(/ \(.*/g, "")
                            .replace(/_\(.*/g, "")
                            .split("_")
                            .join(" ");
                        const artist_data = digestPage(artist_name, parser.parseFromString(html, "text/html"));

                        if (artist_data) {
                            const parent = createListElement("artista: " + artist_name);
                            main.appendChild(parent);

                            if (artist_data["origin"]) {
                                child = document.createElement("ul");
                                child.textContent = "Origin: " + artist_data["origin"];
                                parent.appendChild(child);
                            }

                            if (artist_data["genre"]) {
                                child = document.createElement("ul");
                                child.textContent = "Genre(s): " + artist_data["genre"];
                                parent.appendChild(child);
                            }

                            fetch(
                                `https://cors-anywhere.herokuapp.com/http://ws.audioscrobbler.com/2.0/?method=artist.gettopalbums&artist=${artist}&api_key=5bc7811095f0137df272884f76e80839&format=json`
                            )
                                .then(data => data.json())
                                .then(data => {
                                    if (data && data["topalbums"] && data["topalbums"]["album"][0]) {
                                        child = document.createElement("ul");
                                        child.textContent = "Top Album: " + data["topalbums"]["album"][0]["name"];
                                        parent.appendChild(child);
                                    }
                                });
                        }
                    });
            }
            setTimeout(
                axios.post(URL + "/saveMusicXML", { element: document.getElementById("main").innerHTML }),
                15000
            );
        });
    },
};

(function createEvents() {
    const radios = document.getElementsByName("list");
    for (let i = 0, length = radios.length; i < length; i++)
        radios[i].addEventListener("click", () => {
            main.innerHTML = "";
            ItemFunctionMap[i]();
        });
})();
