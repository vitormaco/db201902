const analiseAndDataMining = [
    "Qual é a média e desvio padrão dos ratings para artistas musicais e filmes?",
    "Quais são os artistas e filmes com o maior rating médio curtidos por pelo menos duas pessoas? Ordenados por rating médio.",
    "Quais são os 10 artistas musicais e filmes mais populares? Ordenados por popularidade.",
    "Crie uma view chamada ConheceNormalizada que represente simetricamente os relacionamentos de conhecidos da turma. Por exemplo, se a conhece b mas b não declarou conhecer a, a view criada deve conter o relacionamento (b,a) além de (a,b).",
    "Quais são os conhecidos (duas pessoas ligadas na view ConheceNormalizada) que compartilham o maior numero de filmes curtidos?",
    "Qual o número de conhecidos dos conhecidos (usando ConheceNormalizada) para cada integrante do seu grupo?",
    "Construa um gráfico para a função f(x) = (número de pessoas que curtiram exatamente x filmes).",
    "Construa um gráfico para a função f(x) = (número de filmes curtidos por exatamente x pessoas).",
    "Defina duas outras informações (como as anteriores) que seriam úteis para compreender melhor a rede. Agregue estas informações à sua aplicação.",
];

const extracaoDeInformacao = [
    "Extração de informação de filmes (A API demora para retornar, mais ou menos 30 segundos porque a chave é FREE)",
    "Extração de informação de Artistas Musicais",
];

// Mapeamento de função para indice.
const mediaEdesvio = "0";
const maiorRatingMedio = "1";
const top10 = "2";
const relacionamentoConhecidos = "3";
const conhecidosANDfilmes = "4";
const numeroDeConhecidos = "5";
const grafPessoaXFilme = "6";
const grafFilmeXPessoa = "7";
const informacoesAdicionais = "8";
const extracaoFilmes = "9";
const extracaoMusicas = "10";

(create => {
    const title = document.createElement("h3");
    title.textContent = "Selecione um item da atividade de Analise e Data Mining: ";
    container.appendChild(title);

    analiseAndDataMining.forEach((element, index) => {
        createBdItem(element, index);
    });
    container.appendChild(document.createElement("br"));
    const extracaoTitle = document.createElement("h3");
    extracaoTitle.textContent = "Selecione um item da atividade de Extração:";
    container.appendChild(extracaoTitle);
    extracaoDeInformacao.forEach((element, index) => {
        createBdItem(element, index + analiseAndDataMining.length);
    });
    container.appendChild(document.createElement("br"));
    const mainContent = document.createElement("div");
    mainContent.setAttribute("id", "main");
    container.appendChild(document.createElement("hr"));
    container.appendChild(mainContent);
})();

function createBdItem(element, index) {
    const radioB = document.createElement("input");
    const text = document.createElement("span");
    text.textContent = element;
    radioB.setAttribute("type", "radio");
    radioB.setAttribute("name", "list");
    radioB.setAttribute("value", index.toString());
    container.appendChild(radioB);
    container.appendChild(text);
    container.appendChild(document.createElement("br"));
}
